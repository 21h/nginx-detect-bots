import re
import signal, os
import errno
import logging
import datetime

FIFO = '/run/nginx-detect-bots.pipe'
LOGFILE = '/var/log/nginx-detect-bots/bots.log'
PIDFILE = '/run/nginx-detect-bots.pid'

def touch(fname, useragent, times=None):
    with open(fname, 'w') as t:
        t.write(useragent)
        t.close()


def startLogger(signum, frame):
    logging.shutdown()
    logging.basicConfig(filename=LOGFILE, level=logging.INFO, format='')
    logging.info("'%s', '%s'", str(datetime.datetime.now()), "Started")


def terminateSignal(signum, frame):
    os.remove(PIDFILE)
    exit(0)


signal.signal(signal.SIGHUP, startLogger)
signal.signal(signal.SIGTERM, terminateSignal)


try:
    os.mkfifo(FIFO)
except OSError as oe:
    if oe.errno != errno.EEXIST:
        raise


try:
    startLogger(None, None)
except OSError as oe:
    if oe.errno != errno.EEXIST:
        raise

try:
    with open(PIDFILE, 'w') as f:
        f.write(str(os.getpid()))
except OSError as oe:
    if oe.errno != errno.EEXIST:
        raise

line_nginx_full = re.compile(
    r"""(?P<ipaddress>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) - - \[(?P<dateandtime>\d{2}\/[a-z]{3}\/\d{4}:\d{2}:\d{2}:\d{2} (\+|\-)\d{4})\] ((\"(GET|POST) )(?P<url>.+)(http\/1\.(1|0)")) (?P<statuscode>\d{3}) (?P<bytessent>\d+) (["](?P<refferer>(\-)|(.+))["]) (["](?P<useragent>.+)["])""", re.IGNORECASE)

while True:
    with open(FIFO) as fifo:
        print("Listening log file")
        while True:
            data = fifo.readline()
            if len(data) == 0:
                # stop cycle if no data
                # break
                pass
            else:
                # here your code
                parsed_line = line_nginx_full.match(data.strip())
                if (parsed_line != None):
                    request = parsed_line.groupdict()
                    if (request['useragent'].lower().find("bot") >= 0):
                        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
                        print("Bot:", request['useragent'])
                        print("TS:", request['dateandtime'])
                        print("IP:", request['ipaddress'])
                        touch(request['ipaddress'], request['useragent'])
                        logging.info("'%s', '%s', '%s', '%s'", str(datetime.datetime.now(
                        )), request['ipaddress'], request['dateandtime'], request['useragent'])
                    else:
                        logging.warning("'%s', '%s'", str(datetime.datetime.now()), "String not recognized")
                        logging.warning("'%s', '%s'", str(datetime.datetime.now()), data)
